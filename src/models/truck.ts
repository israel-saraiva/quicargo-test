import {
  Column,
  CreatedAt,
  DeletedAt,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

@Table
export class Truck extends Model<Truck> {
  @Column
  owner: string;

  @Column
  loadType: string;

  @Column
  year: string;

  @Column
  particulars: string;

  @CreatedAt
  creationDate: Date;

  @UpdatedAt
  updatedOn: Date;

  @DeletedAt
  deletionDate: Date;
}
