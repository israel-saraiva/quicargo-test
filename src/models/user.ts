import {
  AllowNull,
  Column,
  CreatedAt,
  DeletedAt,
  ForeignKey,
  Model,
  Table,
  Unique,
  UpdatedAt,
  BelongsTo,
} from 'sequelize-typescript';

import { Profile } from './profile';

@Table
export class User extends Model<User> {
  @Column
  firstname: string;

  @Column
  lastname: string;

  @Unique
  @AllowNull(false)
  @Column
  email: string;

  @Column
  password: string;

  @Column
  phone: string;

  @ForeignKey(() => Profile)
  @Column
  profileId: number;

  @BelongsTo(() => Profile)
  profile: Profile;

  @Column
  active: boolean;

  @Column
  emailConfirmed: boolean;

  @Unique
  @Column
  googleId: string;

  @Unique
  @Column
  facebookId: string;

  @CreatedAt
  creationDate: Date;

  @UpdatedAt
  updatedOn: Date;

  @DeletedAt
  deletionDate: Date;
}
