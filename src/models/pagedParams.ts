import { WhereOptions } from 'sequelize/types';

export interface PagedParams {
  where?: WhereOptions;
  paginationParams: { page: string; size: string };
  orderParams?: { orderBy: string; order: string };
  searchParams?: any[][];
}
