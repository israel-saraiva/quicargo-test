import { Column, Model, Table } from 'sequelize-typescript';

@Table
export class Profile extends Model<Profile> {
  @Column
  name: string;

  @Column
  code: string;
}
