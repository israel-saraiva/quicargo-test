import {
  Column,
  CreatedAt,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';

import { Truck } from './truck';

@Table
export class TruckLocation extends Model<TruckLocation> {
  @Column
  latitude: string;

  @Column
  longitude: string;

  @ForeignKey(() => Truck)
  @Column
  truckId: number;

  @CreatedAt
  creationDate: Date;
}
