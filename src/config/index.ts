import * as express from 'express';
import { Application } from 'express';
import * as dotenv from 'dotenv';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as cors from 'cors';
import * as passport from 'passport';

import { SequelizeConfig } from './database';
import { AppController } from './routes';
import { configureJwt } from '../auth/jwt';

dotenv.config();

class Server {
  public app: Application;
  public dbConfig: SequelizeConfig;

  constructor() {
    this.app = express();

    this.configureDatabase();
    this.configureMiddlewares();
    this.configureAuthentication();
    this.configureRoutes();
  }

  public configureAuthentication(): void {
    configureJwt();

    this.app.use(passport.initialize());
  }

  public configureDatabase(): void {
    SequelizeConfig.initialize();
  }

  // application config
  public configureMiddlewares(): void {
    const origin = '*'; //'http://localhost:3000'

    const corsOptions: cors.CorsOptions = {
      origin,
      optionsSuccessStatus: 200,
    };

    // express middleware
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());

    this.app.use('/public', express.static(__dirname + '/public'));

    this.app.use(cors(corsOptions));

    this.app.set('port', process.env.API_PORT || 3050);
    this.app.use(morgan('tiny'));

    this.app.disable('etag');

    // cors
    this.app.use((req, res, next) => {
      // Disable caching for content files
      res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
      res.header('Pragma', 'no-cache');
      res.header('Expires', '0');

      res.header(
        'Access-Control-Allow-Methods',
        'GET, POST, PUT, DELETE, OPTIONS'
      );
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials, Access-Control-Allow-Origin, x-access-token'
      );
      res.header('Access-Control-Allow-Credentials', 'true');
      next();
    });
  }

  // application routes
  public configureRoutes(): void {
    this.app.use('/api', AppController());
  }
}

export const App: Application = new Server().app;
