import { cyanBright, greenBright, redBright } from 'chalk';
import { Sequelize } from 'sequelize-typescript';
import { log } from 'util';

import { Profile } from '../models/profile';
import { Truck } from '../models/truck';
import { TruckLocation } from '../models/truckLocation';
import { User } from '../models/user';
import * as Faker from 'faker';

const models = [User, Profile, Truck, TruckLocation];

export class SequelizeConfig {
  public static sequelize: Sequelize;

  static instance() {
    if (this.sequelize) return this.sequelize;

    this.sequelize = new Sequelize({
      database: process.env.POSTGRES_DB,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      port: 5433,
      dialect: 'postgres',
      models,
      logging: false,
    });

    return this.sequelize;
  }

  public static async initialize() {
    try {
      const database = process.env.POSTGRES_DB;

      const logTry = `Trying to connect with database ${database}`;

      log(cyanBright(logTry));

      await this.instance().authenticate();

      const logSuccess = `The connection with ${database} has been established.`;

      log(greenBright(logSuccess));
    } catch (error) {
      console.error(redBright('Unable to connect to the database:'), error);
    }
  }

  static createTrucks() {
    const list = new Array(5).fill(0);

    list.forEach(async () => {
      const truck = await Truck.create({
        owner: `${Faker.name.firstName()} ${Faker.name.lastName()}`,
        loadType: 'dry charge',
        year: '2019/2020',
        particulars:
          'Hub Reduction - No • Third Axle - Rocker • Type of Box - Manual • Body - Mechanical Horse',
      } as Truck);

      list.forEach(() => {
        TruckLocation.create({
          truckId: truck.id,
          latitude: Faker.address.latitude(),
          longitude: Faker.address.longitude(),
        } as TruckLocation);
      });
    });
  }

  public static async up() {
    try {
      await this.instance().authenticate();

      this.sequelize.sync().then(() => {
        log('TEST');
        this.createTrucks();
      });
    } catch (error) {
      console.error(
        redBright(
          'Database migration failed. Unable to connect to the database.'
        ),
        error
      );
    }
  }
}
