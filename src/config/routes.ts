import { Router } from 'express';
import * as passport from 'passport';

import { AuthController } from '../controllers/auth.controller';
import { ProfileController } from '../controllers/profile.controller';
import { TruckController } from '../controllers/truck.controller';
import { TruckLocationController } from '../controllers/truckLocation.controller';
import { UserController } from '../controllers/user.controller';
import { UtilController } from '../controllers/util.controller';

const publicRoutes = [
  { path: '/auth', routes: AuthController() },
  { path: '/truck', routes: TruckController() },
  { path: '/truck-location', routes: TruckLocationController() },
  { path: '/util', routes: UtilController() },
];

const secureRoutes = [
  { path: '/users', routes: UserController() },
  { path: '/profiles', routes: ProfileController() },
];

export const AppController = () => {
  const router = Router();

  publicRoutes.forEach((route) => router.use(route.path, route.routes));

  secureRoutes.forEach((route) =>
    router.use(
      route.path,
      passport.authenticate(['jwt'], { session: false }),
      route.routes
    )
  );

  return router;
};
