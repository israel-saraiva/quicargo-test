import { NextFunction, Request, Response } from 'express';
import { sign, verify } from 'jsonwebtoken';

import { Profile } from '../models/profile';
import { User } from '../models/user';
import { getConfiguration } from './config';

// Generate an Access Token for the given User ID
export const generateAccessToken = (userId: number) => {
  const config = getConfiguration();

  const expiresIn = '10 hours';
  const audience = config.get('authentication.token.audience');
  const issuer = config.get('authentication.token.issuer');
  const secret = config.get('authentication.token.secret');

  const token = sign({}, secret, {
    expiresIn: expiresIn,
    audience: audience,
    issuer: issuer,
    subject: userId.toString(),
  });

  return token;
};

export const generateEmailConfirmationToken = (userId: number) => {
  const config = getConfiguration();

  const audience = config.get('authentication.email.audience');
  const issuer = config.get('authentication.email.issuer');
  const secret = config.get('authentication.email.secret');

  const token = sign({}, secret, {
    audience: audience,
    issuer: issuer,
    subject: userId.toString(),
  });

  return token;
};

export const checkEmailConfirmationToken = (token: string) =>
  new Promise(async (resolve, reject) => {
    try {
      const config = getConfiguration();

      const audience = config.get('authentication.email.audience');
      const issuer = config.get('authentication.email.issuer');
      const secret = config.get('authentication.email.secret');

      const res = verify(token, secret, {
        audience: audience,
        issuer: issuer,
      });

      resolve(res);
    } catch (err) {
      reject(err);
    }
  });

export const isAdmin = (req: Request, res: Response, next: NextFunction) => {
  const { userId } = req as any;

  const ADMIN = 'ADMIN';

  User.findOne({
    where: { id: userId },
  }).then((user) => {
    Profile.findAll().then((profiles) => {
      const hasAdminProfile = profiles.some(
        (p) => p.name.toUpperCase() === ADMIN && user.profileId === p.id
      );

      console.log(userId);

      if (hasAdminProfile) {
        next();
      } else {
        res.status(403).send('Require Admin Role!');
        return;
      }
    });
  });
};
