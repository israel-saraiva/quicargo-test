import * as passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { User } from '../models/user';
import { getConfiguration } from './config';

export const configureJwt = () => {
  const config = getConfiguration();

  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.get('authentication.token.secret'),
    issuer: config.get('authentication.token.issuer'),
    audience: config.get('authentication.token.audience'),
  };

  passport.use(
    new Strategy(jwtOptions, (payload, done) => {
      User.findOne({ where: { id: parseInt(payload.sub) } })
        .then((user) => {
          if (user) {
            return done(null, user, payload);
          }
          return done(new Error('User not found'), false);
        })
        .catch((error: any) => {
          return done(error);
        });
    })
  );
};
