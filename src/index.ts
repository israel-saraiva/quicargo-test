import { green } from 'chalk';
import { App } from './config';

App.listen(App.get('port'), () => {
  console.log(
    `App is running at ${green('http://localhost:%d')} in %s mode`,
    App.get('port'),
    App.get('env')
  );

  console.log('Press CTRL-C to stop\n');
});
