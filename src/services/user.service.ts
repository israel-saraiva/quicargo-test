import { hashSync } from 'bcrypt';

import { User } from '../models/user';
import { BaseService } from './base/base.service';

export const UserService = () => {
  const { all, allPaged, one, remove, update } = BaseService(User);

  const defaultPassword = '123456';

  const create = async (data: any) => {
    const result = await User.create({
      ...(data as Object),
      password: hashSync(defaultPassword, 8),
      emailConfirmed: true,
    });

    return result;
  };

  const resetPass = async (id: number) => {
    const result = await User.update(
      { password: hashSync(defaultPassword, 8) },
      { where: { id } }
    );

    return result;
  };

  return { all, allPaged, one, create, remove, update, resetPass };
};
