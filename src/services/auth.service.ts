import { compareSync, hashSync } from 'bcrypt';

import { generateAccessToken } from '../auth/token';
import { User } from '../models/user';
import { Profile } from '../models/profile';
import { ProfileEnum } from '../models/enum/profile.enum';

type RegisterData = {
  email: string;
  password: string;
};

type LoginData = {
  email: string;
  password: string;
};

export const AuthService = () => {
  const signup = async ({ email, password }: RegisterData) => {
    const foundUser = await User.findOne({
      where: { email },
    });

    if (foundUser) {
      throw 'The user already exists!';
    }

    const profile = await Profile.findOne({
      where: { code: ProfileEnum.User },
    });

    const user = await User.create({
      email,
      password: hashSync(password, 8),
      emailConfirmed: false,
      profileId: profile.id,
    } as User);

    return {
      userId: user.id,
      message: 'User successfully registered!',
    };
  };

  const signin = async ({ email, password }: LoginData) => {
    const user = await User.findOne({
      where: { email },
    });

    if (!user) {
      throw 'Username or Password is invalid!';
    }

    if (!user.emailConfirmed) {
      throw 'Unverified email';
    }

    const passwordIsValid = compareSync(
      password,
      user.password ? user.password : ''
    );

    if (!passwordIsValid) {
      throw 'Username or Password is invalid!';
    }

    const token = generateAccessToken(user.id);

    return { auth: true, accessToken: token, user };
  };

  const getUser = async (userId: string) => {
    const user = await User.findOne({
      where: { id: userId },
    });

    return user;
  };

  return { signup, signin, getUser };
};
