import { TruckLocation } from '../models/truckLocation';
import { BaseService } from './base/base.service';

export const TruckLocationService = () => {
  const allByTruck = async (truckId: number) => {
    return await TruckLocation.findAll({ where: { truckId } });
  };

  return { allByTruck };
};
