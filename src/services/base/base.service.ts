import { Model } from 'sequelize-typescript';
import { FindAndCountOptions } from 'sequelize/types';
import { Op } from 'sequelize';

export const BaseService = <T>(model: (new () => T) & typeof Model) => {
  const all = async () => {
    const result = await model.findAll();
    return result;
  };

  const allPaged = async (options: FindAndCountOptions) => {
    const { offset, limit } = options;

    const page = offset / limit;
    const size = limit;

    if (options.where) {
      const where: any = options.where;
      const whereKeys = Object.keys(where);

      whereKeys.forEach((key) => {
        const props = Object.keys(where[key]);

        if (props.includes('like')) {
          where[key][Op.like] = where[key].like;
          delete where[key].like;
        }
      });
    }

    const result = await model.findAndCountAll(options);

    return { page, size, ...result };
  };

  const one = async (id: number) => {
    const result = await model.findOne({ where: { id } });
    return result;
  };

  const create = async (data: any) => {
    const result = await model.create(data as Object);
    return result;
  };

  const update = async (id: number, data: any) => {
    const result = model.update(data as Object, { where: { id } });
    return result;
  };

  const remove = async (id: number) => {
    await model.destroy({ where: { id } });
  };

  return { all, allPaged, one, create, update, remove };
};

type CustomMethods = {
  [methodName: string]: Function;
};

export type GenericMethods = ReturnType<typeof BaseService> | CustomMethods;

export type BaseServiceModel = typeof BaseService;
