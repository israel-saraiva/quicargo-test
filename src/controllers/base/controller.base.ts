import { redBright } from 'chalk';
import { Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import { Model } from 'sequelize-typescript';
import { FindAndCountOptions, WhereOptions } from 'sequelize/types';

import { PagedParams } from '../../models/pagedParams';
import { BaseServiceModel } from '../../services/base/base.service';

export const ControllerBase = <T>(
  model: (new () => T) & typeof Model,
  service: BaseServiceModel
) => {
  const all = async (req: Request, res: Response) => {
    try {
      const baseService = service(model);
      const result = await baseService.all();

      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API>ALL>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  const allPaged = async (req: Request, res: Response) => {
    try {
      const {
        where,
        paginationParams,
        orderParams,
        searchParams,
      }: PagedParams = req.body;

      const options: FindAndCountOptions = {};

      if (!!paginationParams) {
        const { page, size } = paginationParams;

        const pageNumber = parseInt(page);
        const sizeNumber = parseInt(size);

        options.offset = pageNumber * sizeNumber;
        options.limit = sizeNumber;
      }

      if (!!orderParams) {
        const { order, orderBy } = orderParams;
        options.order = [[orderBy, order]];
      }

      if (!!searchParams) {
        const search: WhereOptions = {};

        if (!!searchParams) {
          searchParams.map((param) => {
            search[param[0]] = { like: `%${param[1]}%` };
          });
        }

        options.where = { ...where, ...search };
      }

      const baseService = service(model);
      const result = await baseService.allPaged(options);
      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API ALLPAGED ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  const one = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const baseService = service(model);
      const result = await baseService.one(parseInt(id as string));
      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API>GETONE>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  const create = async (req: Request, res: Response) => {
    try {
      const baseService = service(model);
      const result = await baseService.create(<Object>req.body);
      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API>CREATE>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  const update = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const baseService = service(model);
      const result = await baseService.update(
        parseInt(id as string),
        <Object>req.body
      );
      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API>UPDATE>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  const remove = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const baseService = service(model);
      await baseService.remove(parseInt(id as string));
      res.status(HttpStatus.NO_CONTENT).end();
    } catch (error) {
      console.log(redBright('API>REMOVE>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  return { all, allPaged, one, create, update, remove };
};
