import { Request, Response, Router } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as passport from 'passport';

import { generateAccessToken } from '../auth/token';
import { AuthService } from '../services/auth.service';

export const AuthController = () => {
  const router = Router();
  const authService = AuthService();

  const signup = async (req: Request, res: Response) => {
    try {
      const result = await authService.signup(req.body);
      res.status(HttpStatus.OK).send(result);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error);
    }
  };

  const signin = async (req: Request, res: Response) => {
    try {
      const result = await authService.signin(req.body);
      res.status(HttpStatus.OK).send(result);
    } catch (error) {
      res.status(HttpStatus.UNAUTHORIZED).send(error);
    }
  };

  const getUser = async (req: Request, res: Response) => {
    try {
      const { userId } = req.params;
      const result = await authService.getUser(userId);
      res.status(HttpStatus.OK).send(result);
    } catch (error) {
      console.log('AUTH GET USER ERROR =>', error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error);
    }
  };

  const verifyToken = async (req: Request, res: Response) => {
    try {
      const { user } = req;
      if (user) return res.status(HttpStatus.OK).send({ auth: true, user });
      else return res.status(404).send({ auth: false });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error);
    }
  };

  const generateUserToken = async (req: Request, res: Response) => {
    try {
      const { user } = req as any;

      const token = generateAccessToken(user.id);
      const frontUrl = process.env.FRONTEND_URL;

      res.redirect(`${frontUrl}?token=${token}`);
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error);
    }
  };

  router.post('/signup', signup);
  router.post('/signin', signin);
  router.get('/user/:userId', getUser);

  router.get(
    '/verify-token',
    passport.authenticate('jwt', { session: false }),
    verifyToken
  );

  router.get(
    '/google',
    passport.authenticate('google', {
      session: false,
      scope: ['openid', 'profile', 'email'],
    })
  );

  router.get(
    '/google/callback',
    passport.authenticate('google', { failureRedirect: '/', session: false }),
    generateUserToken
  );

  router.get(
    '/facebook',
    passport.authenticate('facebook', {
      session: false,
    })
  );

  router.get(
    '/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/',
      session: false,
    }),
    generateUserToken
  );

  return router;
};
