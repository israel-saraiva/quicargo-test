import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';

import { SequelizeConfig } from '../config/database';

export const UtilController = () => {
  const router = Router();

  const generateFakeData = async (req: Request, res: Response) => {
    await SequelizeConfig.up();
    res.status(HttpStatus.OK).json('Fake data was successfully generated!');
  };

  router.get('/fake-data', generateFakeData);

  return router;
};
