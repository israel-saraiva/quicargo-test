import { Router } from 'express';

import { Truck } from '../models/truck';
import { BaseService } from '../services/base/base.service';
import { ControllerBase } from './base/controller.base';

export const TruckController = () => {
  const router = Router();

  const { all, one, create, update, remove } = ControllerBase<Truck>(
    Truck,
    BaseService
  );

  router.get('/', all);
  router.get('/:id', one);
  router.post('/', create);
  router.put('/:id', update);
  router.delete('/:id', remove);

  return router;
};
