import * as Chalk from 'chalk';
import { Request, Response, Router } from 'express';
import * as HttpStatus from 'http-status-codes';

import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { ControllerBase } from './base/controller.base';

export const UserController = () => {
  const router = Router();

  const { all, allPaged, one, create, update, remove } = ControllerBase<User>(
    User,
    UserService
  );

  const resetPass = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;

      const userService = UserService();
      const result = await userService.resetPass(parseInt(id as string));
      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(Chalk.redBright('MSAUTH RESETPASS ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  router.get('/all', all);
  router.post('/allPaged', allPaged);
  router.get('/one/:id', one);
  router.post('/create', create);
  router.put('/update/:id', update);
  router.delete('/remove/:id', remove);
  router.put('/resetPass/:id', resetPass);

  return router;
};
