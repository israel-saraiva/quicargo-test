import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import { redBright } from 'chalk';

import { TruckLocation } from '../models/truckLocation';
import { TruckLocationService } from '../services/truckLocation.service';
import { ControllerBase } from './base/controller.base';
import { BaseService } from '../services/base/base.service';

export const TruckLocationController = () => {
  const router = Router();

  const { create, update } = ControllerBase<TruckLocation>(
    TruckLocation,
    BaseService
  );

  const allByTruck = async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const service = TruckLocationService();
      const result = await service.allByTruck(parseInt(id));

      res.status(HttpStatus.OK).json(result);
    } catch (error) {
      console.log(redBright('API>ALL>ERROR ==>'), error);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error });
    }
  };

  router.get('/:id', allByTruck);
  router.post('/', create);
  router.put('/:id', update);

  return router;
};
