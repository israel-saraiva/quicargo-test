import { Router } from 'express';

import { Profile } from '../models/profile';
import { BaseService } from '../services/base/base.service';
import { ControllerBase } from './base/controller.base';

export const ProfileController = () => {
  const router = Router();

  const { all, allPaged, one, create, update, remove } = ControllerBase<
    Profile
  >(Profile, BaseService);

  router.get('/all', all);
  router.post('/allPaged', allPaged);
  router.get('/one/:id', one);
  router.post('/create', create);
  router.put('/update/:id', update);
  router.delete('/remove/:id', remove);

  return router;
};
