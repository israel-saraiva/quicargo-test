# Test

In this exercise you are going create a Rest full API, that will be responsible for saving and
listing trucks data as following

- Create and Edit a Truck
- View a Truck
- List all Trucks
- Create/save Truck Location (latitude/longitude)
- List all locations of a truck
  Technical Requirements
- You must use the NodeJS in backend (Yes, if you don’t have any experience - Use the
  Internet :)):
- You must save the data in a database.
- You can use other libraries to help if you want, ex:
- Express, Sequelize, etc
- Build System (Gulp/Grunt/NPM)

# Tech used:

- NodeJs
- Express
- Postgree Database
- Typescript
- Webpack
- Sequelize

This project includes authentication by jwt token using passport, functionality that I've implemented before to another project.

# Steps to execute

1. Execute this command in a terminal: docker-compose up database
2. In another terminal execute: yarn start
3. Make a GET request to: "http://localhost:3050/api/util/fake-data" to generate fake data

### Endpoints

GET http://localhost:3050/api/truck - List all trucks\
GET http://localhost:3050/api/truck/1 - View a truck by ID\
PUT http://localhost:3050/api/truck/1 - Edit a truck
POST http://localhost:3050/api/truck - Create a truck

```javascript
Example: {
    "owner": "Israel Saraiva",
    "loadType": "dry charge",
    "year": "2019/2020",
    "particulars": "Hub Reduction - No • Third Axle - Rocker • Type of Box - Manual • Body - Mechanical Horse"
}
```

GET http://localhost:3050/api/truck-location/2 - Get all locations by truck ID\
PUT http://localhost:3050/api/truck-location/10 - Update a truck location by ID\
POST http://localhost:3050/api/truck-location - Create location

```javascript
Example: {
    "truckId": 2,
    "latitude": "-26.4225",
    "longitude": "50.5538"
}
```

# Steps to deploy

### Environment configuration

- Install pm2 on ec2 insntance\
  npm i pm2 -g

- Install filezilla client on your machine\
  Go to Edit>Settings>SFTP and add the file secret.pem\
  Add new site on filezilla with host: ec2-52-15-95-202.us-east-2.compute.amazonaws.com\
  user: ubuntu\
  Then click connect.

- Connect via ssh and give propper permissions to user ubuntu\
  sudo chown -R ubuntu:ubuntu /home/ubuntu

- Transport build files\
  copy build files (frontend / backend) to ubuntu folder

- Config NGINX\
  Point nginx to application front end folder:\
  root /home/ubuntu/projects/quicargo-fe/build;\

Create a location and point to backend service

```javascript
location /api {
proxy_pass http://localhost:3050;
}
```

Close file, save and restart nginx:\
systemctl restart nginx

- Deploy backend:\
  Use pm2 to deploy backend:\
  pm2 start ./dist/webapi.js --watch=true --name 'quicargo-api'
