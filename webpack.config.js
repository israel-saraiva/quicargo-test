const fs = require('fs');
const NodemonPlugin = require('nodemon-webpack-plugin');
const path = require('path');

const nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  devtool: 'source-map',
  entry: {
    server: './src/index.ts',
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
      },
    ],
  },

  output: {
    filename: 'webapi.js',
    path: path.join(__dirname, 'dist/'),
  },

  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
  },
  plugins: [new NodemonPlugin()],
  target: 'node',
  externals: nodeModules,
};
