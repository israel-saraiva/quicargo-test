FROM node:alpine

WORKDIR /usr/quicargo-api

COPY package*.json ./
RUN yarn install --force && yarn cache clean

COPY . .

EXPOSE 3010

CMD ["yarn", "start"]